/*
 * Copyright (C) 2021  Aloys Liska
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * importWithContentHub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "filemngt.h"
//---------------------
FileMngt::FileMngt() 
{

}

//------------------------------------
bool FileMngt::exists(QString source)
{
    if (source.isEmpty())
        return false;

    if(source.startsWith("file://"))
        source.remove("file://");

    QFile file(source);
    return file.exists();
}

//-----------------------------------------------------
QString FileMngt::fileCompleteBaseName(QString source)
{
    if (source.isEmpty())
        return "";

    if(source.startsWith("file://"))
        source.remove("file://");

    QFileInfo fi(source);
    return fi.completeBaseName();
}

//------------------------------------
bool FileMngt::remove(QString source)
{
    if (source.isEmpty())
        return false;

    if(source.startsWith("file://"))
        source.remove("file://");

    QFile file(source);
    return file.remove();
}

//-------------------------------------------------------------
bool FileMngt::rename(QString source, const QString &fileName)
{
    if (source.isEmpty())
        return false;

    if(source.startsWith("file://"))
        source.remove("file://");

    QFile file(source);
    return file.rename(fileName);
}

//---------------------------------------
QString FileMngt::suffix(QString source)
{
    if (source.isEmpty())
        return "";

    if(source.startsWith("file://"))
        source.remove("file://");

    QFileInfo fi(source);
    return fi.suffix();
}


// json object and file management
//-------------------------------------------------------------
bool FileMngt::loadJsonGameState(QString key, bool defaultVal)
{
    // load data from json in gameDataObj if not done yet
    if (gameDataObj.isEmpty())
    {
        if (!loadJsonFile())
            return defaultVal;  // if fail to load json file, then return defaultVal
    }
    
    // load val of key from json object
    QJsonValue val = gameDataObj.value(key);
    if (val.isBool())
        return val.toBool();
    else
    {
        qWarning() << "ERROR: json value is not of expected type bool - at key: " << key;   
        return defaultVal;
    }
}

//-----------------------------------------------------------
int FileMngt::loadJsonGameState(QString key, int defaultVal)
{
    // load data from json in gameDataObj if not done yet
    if (gameDataObj.isEmpty())
    {
        if (!loadJsonFile())
            return defaultVal;  // if fail to load json file, then return defaultVal
    }
    
    // load val of key from json object
    QJsonValue val = gameDataObj.value(key);
    if (val.isDouble())
        return val.toInt();
    else
    {
        qWarning() << "ERROR: json value is not of expected type double - at key: " << key;   
        return defaultVal;
    }
}

//----------------------------------------------------
QVariant FileMngt::loadJsonGameState(QString key)
{
    QVariant emptyVar;
    
    // load data from json in gameDataObj if not done yet
    if (gameDataObj.isEmpty())
    {
        if (!loadJsonFile())
            return emptyVar;  // if fail to load json file, then return empty QVariant
    }
    
    // load val of key from json object
    QJsonValue val = gameDataObj.value(key);
    
    if (val.isArray())
        return val.toVariant();
    else
    {
        qWarning() << "ERROR: json value is not of expected type array - at key: " << key;
        return emptyVar;
    }
}

//------------------------------------------------------
bool FileMngt::saveJsonGameState(QString key, bool val)
{
    // writing "key": val in json object
    gameDataObj.insert(key,val);
    return true;
}

//-----------------------------------------------------
bool FileMngt::saveJsonGameState(QString key, int val)
{
    // writing "key": val in json object
    gameDataObj.insert(key,val);
    return true;
}

//--------------------------------------------------------------
bool FileMngt::saveJsonGameState(QString key, QVariantList val)
{
    // convert val in a json array
    QJsonArray valjsonArray = QVariant::fromValue(val).toJsonArray();

    // writing "key": array val in json object
    gameDataObj.insert(key,valjsonArray);
    
    return true;
}

//----------------------------
bool FileMngt::saveJsonFile()
{
    // app data directory
    QDir appDataDir(appDataPath);
    if(!appDataDir.exists())
    {
        appDataDir.mkpath(appDataPath);
    }
    
    // app data file path
    QFileInfo appDataFilePath(appDataDir, appDataFileName);
    
    // app data file opening
    QFile file(appDataFilePath.filePath());
    if (!file.open(QIODevice::WriteOnly)) 
    {
        qWarning("ERROR: Fail to save json file");   
        return false;
    }

    // write json gameDataObj in file
    file.write(QJsonDocument(gameDataObj).toJson());
    
    return true;
}

//----------------------------
bool FileMngt::loadJsonFile()
{
    // app data directory
    QDir appDataDir(appDataPath);
    if(!appDataDir.exists()) 
    {
        qWarning() << "ERROR: Fail to load json file - directory does not exit: " << appDataPath;   
        return false;
    }
    
    // app data file path
    QFileInfo appDataFilePath(appDataDir, appDataFileName);
    
    // app data file opening
    QFile file(appDataFilePath.filePath());
    if (!file.open(QIODevice::ReadOnly)) 
    {
        qWarning("ERROR: Fail to load json file");   
        return false;
    }
    
    // read file
    QByteArray fileData = file.readAll();
    
    // store in json object gameDataObj
    QJsonDocument jsonDoc = QJsonDocument::fromJson(fileData);
    gameDataObj = jsonDoc.object();
    
    return true;
}

/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "taquin.js" as Taquin


Page {
    id: mainPage
    visible: false
    
    header: PageHeader {
        id: headerMainPage
        title: i18n.tr('Taquin')
        
        contents: Item { 
            id: mainHeaderText
            anchors.fill: parent
            Label {
                id: labelTitle
                text: i18n.tr('Taquin')
                textSize: Label.XLarge
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
            }
            Item {  // use item to center Label between labelTitle and mainHeaderActionBar
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: labelTitle.right
                anchors.right: parent.right
                height: parent.height
                
                Label {
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                    
                    property string winText
                    
                    text: {
                        if (gameOver)
                            winText = '. ' + i18n.tr('WIN!')
                        else
                            winText = ""

                        if (movcount > 1) {
                            // plural text
                            return (movcount + ' ' + i18n.tr('moves') + winText)
                        }
                        else {
                            // singular text
                            return (movcount + ' ' + i18n.tr('move') + winText)
                        }
                    }
                }
            }
        }
        
        trailingActionBar {
            id: mainHeaderActionBar
            actions: [
                Action {
                    iconName: "insert-image"
                    text: i18n.tr('Import image')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("ImgImportPage.qml"))
                    }   
                },
                Action {
                    iconName: "reload"
                    text: i18n.tr('New game')
                    onTriggered: {
                        Taquin.newGame()
                    }   
                },
                Action {
                    iconName: "settings"
                    text: i18n.tr('Settings')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
                    }
                },
                Action {
                    iconName: "info"
                    text: i18n.tr('Information')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("InfoPage.qml"))
                    }  
                }
            ]
            numberOfSlots: 3
        }
    }
    
    Item {
        id:itemTable
        property var temp: 0
        property var imageHeight: parent.height - headerMainPage.height
        
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: headerMainPage.bottom
        anchors.topMargin: {
            if ((squareTile == true) && (imageHeight > parent.width))
                return ((imageHeight - parent.width)/2)
            else
                return 0
        }
        
        height: { 
            if (squareTile == true) {
                if (imageHeight > parent.width)
                    return parent.width
                else
                    return imageHeight
            }
            else
                return imageHeight
        }
        width:  {
            if (squareTile == true) {
                if (imageHeight > parent.width)
                    return parent.width
                else
                    return imageHeight
            }
            else
                return parent.width
        } 
        
        // double click in empty cell, will display numbers in the tiles
        MouseArea {
            anchors.fill: parent
            
            onDoubleClicked: {
                dispNumb = !dispNumb
            }
        }
        
        // The core of taquin: the matrix of tiles
        Repeater {
            id: mouleAgaufre
            model: { 
                // console.log("model=", tabTsize)     
                /* FIXME we can see that when starting the game there is three logs. 
                *  This is probably due to bindings. No consequences but not clean.
                *  tabTsize depends on maxRow and maxCol. So when changing both (from startup or from SettingsPage)
                *  this cause two updates of mouleAgaufre, but one update would be enough.
                */
                return tabTsize
            }
            Tile { 
                n: index
                tileVal: tileTable[index]
                
                property int tmp: 0
                
                onActionTile: {
                    tmp = n
                    Taquin.moveSelectedTile(coord, movTile)
                    if ((movTile[0] != 0) || (movTile[1] != 0)) {
                        mouleAgaufre.itemAt(initFreeIndex).n = tmp     // to move the free tile
                    }
                    // check game  over at each tile move
                    gameOver = Taquin.checkForCompleted()
                    if (gameOver)
                        mouleAgaufre.itemAt(initFreeIndex).visible = true   // make visible the free tile
                }
            }
            
            Component.onCompleted: {
                // check game over for app start
                gameOver = Taquin.checkForCompleted()
                if (gameOver)
                    mouleAgaufre.itemAt(initFreeIndex).visible = true   // make visible the free tile
            }
        }
    }
}

/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3
import "taquin.js" as Taquin

Item {
    id: tileType
    
    /* Tile has coordinates in a matrix form and in a table form: 
     *  - in a matrix: (i,j)
     *      row    i: from 0 to maxRow
     *      column j: from 0 to maxCol
     *  - in a table: n
     *      range: from 0 to tabTsize=maxRow*maxCol-1
     *  
     *      Example within a matrix 4x4:    
     *          j  0  1  2  3
     *      i 
     *      0  [  0  1  2  3 ]
     *      1  [  4  5  6  7 ]
     *      2  [  8  9 10 11 ]
     *      3  [ 12 13 14 15 ]
     *
     * Tile has a value: tileVal = tileTable[n]
     *      
     */
    
    // tile's value
    property int tileVal
    
    // coordinate of the tile in table 
    property int n
    
    // coordinate of tile in matrix: i = coord[0], j = coord[1]
    property var coord: [Math.floor(n/maxCol), n%maxCol]
    
    /* movement to apply to the tile, 
        - 1st element is movement along row
        - 2nd element is movement along column
        - value for each element can be -1, 0 or 1
        - movement (value -1 or 1) is only along row or only along column, so for example [1,1] is not authorized
    */
    property var movTile : [0,0]
  
    property alias tileLabel: label.text
    
    // TODO: probably correction todo here to have actual square when square tile is selected
    width: parent.width/maxCol
    height: parent.height/maxRow
    x: coord[1]*width 
    y: coord[0]*height
    property var radTile: Math.max(0.02*Math.min(tileType.height,tileType.width),units.gu(1))
    
    clip: true
    
    //Add rounded corner to the tile by using opacity mask with a rounded rectangle
    layer.enabled: borderTile
    layer.effect: OpacityMask {
        maskSource: Item {
            width: tileType.width
            height: tileType.height
            Rectangle {
                anchors.fill: parent
                width: tileType.width
                height: tileType.height
                radius: tileType.radTile
            }
        }
    }
    
    // visibility of the tile, the tile with value tileVal=tabTsize-1 is the one which is invisible
    visible: {
        if (tileVal != tabTsize - 1)  {
            return true 
        }
        else
        {
            return false
        }
    }
    
    opacity: {  // to display progressively the hidden tile when game is over
        if (visible == true)
            return 1.0
        else
            return 0
    }
    
    signal actionTile()
    
    
    Image {
        id: imagetileVal
        visible: {
            if (indexImg != 0)
                return true
            else
                return false
        }
        x: - tileVal%maxCol * parent.width
        y: - Math.floor(tileVal/maxCol) * parent.height
        height: tileType.height * maxRow + 1    // the +1 is a trick to avoid a white line appearing sometimes at the tiles including the bottom of the image
        width: tileType.width * maxCol
        fillMode: Image.PreserveAspectCrop
        source: {
            if (visible && indexImg < indexMyImage)
                return ("graphics/AloysLiska_defaultIMG_taquin_0"+indexImg+".JPG")
            else {
                // the user selected image
                if (myImageUrl == "")
                    return ("")
                else
                    return ("file://" + myImageUrl)
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: {
            if (imagetileVal.visible)
                return "transparent" 
            
            if (Theme.name == "Ubuntu.Components.Themes.SuruDark")
                return "maroon"
            else
                return "lightsalmon"
        }
        border.color: {
            if (Theme.name == "Ubuntu.Components.Themes.SuruDark") {
                if (imagetileVal.visible)
                    return theme.palette.normal.base
                else
                    return "lightsalmon"
            }
            else {
                if (imagetileVal.visible)
                    return theme.palette.normal.baseText
                else               
                    return "maroon"
            }
        }
        
        // Add a border to the tile
        border.width: borderTile ? Math.max(0.01*Math.min(tileType.height,tileType.width),1) : 0
        radius: borderTile ? tileType.radTile : 0
        
        // Display Number in the tile
        Label {
            id: label
            visible: !imagetileVal.visible || dispNumb
            anchors.centerIn: parent
            text: tileVal
            font.family: "Helvetica"
            font.bold: true
            textSize: Label.XLarge
            color: theme.palette.normal.backgroundText
        }
    }
    
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        property var mousePress: [0,0]      // (x,y) coordinates on mouse pressed
        property var mouseMove:  [0,0]      // (x,y) mouse move (from pressed to released)   
        
        onPressed: {
            // Detect possible move
            movTile = Taquin.CalcPossibleMove(coord)
            
            // Memorize mouse position on press
            mousePress[0] = mouseX
            mousePress[1] = mouseY
        }
        
        onReleased: {
            // Calculate mouse movement from pressed to released
            mouseMove[0] = mouseX - mousePress[0]
            mouseMove[1] = mouseY - mousePress[1]
                        
           // Compare mouse movement with possible move
           if (Taquin.compareMoves(mouseMove,movTile) == true) {
                console.log("Tile ", n,":", coord, "movTile=", movTile, " tileVal=", tileVal)
                tileType.actionTile()
                n = maxCol * (coord[0] + movTile[0]) + coord[1] + movTile[1]
            }
        }
    }
    
   
    Behavior on x {
        PropertyAnimation {
            duration: 150
        }
    }
    
    Behavior on y {
        PropertyAnimation {
            duration: 150
        }    
    }

   Behavior on opacity {
        PropertyAnimation {
            duration: { 
                if (gameOver)
                    return 1000
                else
                    return 1
            }
            easing {type: Easing.InCubic}
        }    
    }
    
}
